from django.contrib import admin
from . models import *

# Register your models here.

class TaskAdmin(admin.ModelAdmin):
    list_display              = ['id', 'user', 'title', 'complete', 'created_at']
    ordering                  = ['complete']

admin.site.register(Task, TaskAdmin)
